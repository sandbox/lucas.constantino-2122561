<?php

/**
 * @file
 * Provide some private functions.
 */

/**
 * Submit callback for taxonomy vocabulary form.
 */
function _taxonomy_sort_taxonomy_form_vocabulary_submit($form_id, &$form_state) {
  // @TODO: hook_invocation should be done in other phase, and the results be cached out.
  $sorting_methods = module_invoke_all('taxonomy_sort');

  // Simplify the use of the options.
  $sorting_options = $form_state['values']['taxonomy_sort'];

  // Set the persistence of form options.
  // @TODO: Save data to a custom database table, not a generic one.
  $persistent_variables = array(
    'options' => $sorting_options,
    'sorting_method' => '<default>',
  );

  // If a sorting method is defined.
  if (isset($sorting_methods[$sorting_options['sort_method']])) {

    // Hold the method in usage.
    $sorting_method = $sorting_methods[$sorting_options['sort_method']];

    // Persist the sorting method.
    $persistent_variables['sorting_method'] = $sorting_method;

    // Make the options leaner.
    unset($sorting_options['sort_method']);

    // Execute the sorting.
    // This call can be done out this scope, as soon as it receives the same arguments.
    _taxonomy_sort_reorder(
      $form_state['vocabulary'],
      $sorting_options,
      $sorting_method
    );
  }

  // Save the persistent variables.
  variable_set($form_state['vocabulary']->vid . '_taxonomy_sort', $persistent_variables);
}

/**
 * Form builder to confirm resetting a vocabulary to sorting order.
 */
function _taxonomy_sort_confirm_reset_sorting_form($form, &$form_state, $vocabulary) {

  // Set vocabulary reference.
  $form['#vocabulary'] = $vocabulary;

  // Get the current settings for this vocabulary.
  $current_settings = variable_get($form['#vocabulary']->vid . '_taxonomy_sort');
  $sorting_method = $current_settings['sorting_method'];

  // Set submition.
  $form['#submit'][] = '_taxonomy_sort_confirm_reset_sorting_submit';

  // Define some translatable variables to be used on various texts.
  $translatable_variables = array(
    '%title' => $vocabulary->name,
    '!method' => $sorting_method['title'],
  );

  // Set the form confirmation question.
  $question_text = 'Are you sure you want to reset the vocabulary %title to "!method" order?';
  $question = t($question_text, $translatable_variables);

  // Set the form's submition path.
  $path = 'admin/structure/taxonomy/' . $vocabulary->machine_name . '/sort';

  // Set a description.
  $description = t('Resetting the vocabulary term order will discard all custom ordering and sort items again.');

  // Set the YES and CANCEL buttons text.
  $yes = t('Reset by "!method"', $translatable_variables);
  $no = t('Cancel');

  return confirm_form($form, $question, $path, $description, $yes, $no);
}

/**
 * Submit handler to reset a vocabulary to sorting order after confirmation.
 */
function _taxonomy_sort_confirm_reset_sorting_submit($form, &$form_state) {

  // Check if we received a confirmation form.
  if (!empty($form_state['values']['confirm']) && !empty($form['#vocabulary'])) {

    // Get the current settings for this vocabulary.
    $current_settings = variable_get($form['#vocabulary']->vid . '_taxonomy_sort');

    // Make sure the options for sorting are really set.
    if (!empty($current_settings)) {

      // Execute the sorting.
      // This call can be done out this scope, as soon as it receives the same arguments.
      _taxonomy_sort_reorder(
        $form['#vocabulary'],
        $current_settings['options'],
        $current_settings['sorting_method']
      );
    }

    // Redirect to vocabulary list page.
    $form_state['redirect'] = 'admin/structure/taxonomy/' . $form['#vocabulary']->machine_name;
  }
}

/**
 * Main reorderer sorting method. Always use this as a tarting point.
 */
function _taxonomy_sort_reorder($vocabulary, $options, $sorting_method) {

  // Simplify custom method options, if any.
  if (!empty($options['sort_method']) && isset($options[$options['sort_method'] . '_custom_options'])) {
    $options['custom_options'] = $options[$options['sort_method'] . '_custom_options'];
    unset($options[$options['sort_method'] . '_custom_options']);
  }

  // Get the vocabulary, if given key is not object.
  if (!is_object($vocabulary)) {
    // Load via vid or machine name, depending on given value.
    $vocabulary = is_numeric($vocabulary) ? taxonomy_vocabulary_load($vocabulary) : taxonomy_vocabulary_machine_name_load($vocabulary);
  }

  // Get the tree in the current order.
  $taxonomy_tree = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE);

  // Set the new order.
  $taxonomy_tree = call_user_func($sorting_method['sort callback'], $taxonomy_tree, $options, $sorting_method);

  // Define ASC/DESC based on reversion.
  $increment = empty($options['sort_reverse']) ? 1 : -1;

  // The new weight to be set to ther terms.
  $weight = 0;

  // Hold drupal messages till here to avoid printing to many of them.
  $old_status_messages = drupal_get_messages();

  // Change the weights.
  foreach ($taxonomy_tree as $term) {
    $term->weight = $weight;
    $weight += $increment;
    taxonomy_term_save($term);
  }

  // Restore message that were defined before the terms save event.
  $_SESSION['messages'] = $old_status_messages;

  // Tell user about the successful reordering.
  $text = 'The vocabulary "!vocabulary" terms have been reordered by the "!method" method.';
  $message = t($text, array(
    '!vocabulary' => $vocabulary->name,
    '!method' => $sorting_method['title'],
  ));
  drupal_set_message($message, 'status');
}
