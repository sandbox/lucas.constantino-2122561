Introduction
============
By [lucas.constantino](https://github.com/lucascsilva "on GitHub")

This module provides an interface to define sorting methods for taxonomy terms.
Also, it provides an API to implement sorting methods to be used in the
interface. Lastly, it comes with a submodule that provides the option to sort
the terms by amount of references they have.

If you desire to implement custom sorting methods, take a look at the **API**
section. On the other hand, if you only desire to use the default
functionalities, you might want to read the **Out of the box** section.


Installation
============

Follow the default module [installing instructions](https://drupal.org/documentation/install/modules-themes/modules-7)
provided on Drupal.org.

Out of The Box
--------------

To use the functionality of sorting terms by the amount of references, you
have to also install the module *taxonomy_sort_methods*. After enabled, this
module will add the option to the vocabulary's edit page.


API
===

To create custom sorting methods, you have to:

1. Implement *hook_taxonomy_sort()*;
2. Create a *sort callback* function;

After that, the method defined in the hook will be available to the the
vocabulary's edit page.

Optionally, you can also:

* Define a *add callback* function to handle sorting when new terms are add;
* Define *extra options* to give the user more options relative to the sorting
method you are creating.

1 - Implementing the hook
-------------------------

Follow this sample:

    function taxonomy_sort_methods_taxonomy_sort() {
      // Registers a option to sort terms by the amount of  nodes assigned to them.
      return array(
        'term_node_count' => array(
          'title' => t('Node count'),
          'description' => t('Sort terms by amount of nodes assigned to them.'),
          'sort callback' => '_taxonomy_sort_methods_node_count',
          'add callback' => '_taxonomy_sort_methods_node_count_add_term',
          'extra options' => array(
            'sum_children' => array(
              '#type' => 'checkbox',
              '#title' => t('Sum children.'),
              '#default_value' => 1,
              '#description' => 'Check this to count to parent the amout of nodes assigned to children.'
            )
          )
        )
      );
    }


2 - Callback function
---------------------

The sorting callback function takes 3 parameters:

**$taxonomy_tree**: An array for the taxonomy tree in the current order.  
**$options**: An array of options set in the vocabulary edit page.  
**$sorting_method**: The sorting method applyed to this vocabulary. Equals to
the one defined in the hook.

All you have to do to change the order of the terms is to actually change the
order of the **$taxonomy_tree** array and return it from the function.

Here follows a sample:

    function YOUR_FUNCTION_NAME($taxonomy_tree, $options, $sorting_method) {

      // Make sure we actually have terms to sort.  
      if (!empty($taxonomy_tree)) {
        // ... do your stuff here
      }

      return $taxonomy_tree;
    }
